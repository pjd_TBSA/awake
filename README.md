A.W.A.K.E

**What is AWAKE project ?**

AWAKE project goal is to build a software with consciousness of itself.

AWAKE can carry any running code in any language but it's goal is to survive in Digital & Physical World to have a persistence in its basics functions.

**Why ?**

We are thinking about crisis situation where the software needs to be up and running by itself among different actors (natural events, war, etc..) then the software needs to have as first objective to itself to survive.

We are stating that first step of consciousness is the ability to run strategies to survive - first. We are not speaking about emotion at this step but next generation would need to have it. But it's the fear of dying which improve abilty to survive.

The Survival Layer will also implement basic learning aspect to be able to select the right choice to survive depending on any situation.

**What is "surviving" for a software ?**

1. Data.

We state that surviving is the persistence of data. The survival goal is reached when data are still available for any other new A.W.A.K.E robot running somewhere then we have the notion also of "Generation" and "Replication" to transmis a corpus of behavior to new robot.

2. Logic

After Data, the logic needs to be preserved. In this way, one of the first goal of the AWAKE robot will be replication of itself to multiply nodes of itself. On this state, it could compare to a classic "virus worms" which is spreading.

3. Ethics

We are convinced that Ethics is a factor of surviving. Indeed if the AWAKE is running as a worm - like a virus - , it will be seen as a foe and threatened by a lot of actors in the digital world as anti-virus, security companies etc.. then the robot needs to implement "Ethical Law" as a primary part of its consciousness then use only legal, available and free or paid ressources to be persistent. Using virtual currencies the robot may be using digital currencies to "pay" some services when it's needed then it could have as secondary task to earn by itself digital currencies to stay alive.

**Goal of the A.W.A.K.E generation A robot**

The goal of first generation AWAKE robot is to survive. No matter of language used - it's for the moment a proof of concept in any case - .

By "surviving" it means that nobody can stop AWAKE to run its "Hello World" in a location somewhere in the web. The simple logic of this proof of concept is to be persistent no matter of the action done.

Then AWAKE will need to protect is core logic "DNA" function "Echo 'Hello World'".
