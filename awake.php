<?php

/*   AWAKE GENERATION A
 *   SURVIVAL SCRIPT
 *   Pierre Jean Duvivier - ThirdBrain SA (https://thirdbrain.ch)
 *   pierre.jean.duvivier@thirdbrain.ch
 *   Script needs to be simple as possible using procedural approach first
 *   Script needs to be "technical greed" using basic function and able to spread in different language
 */

function logRun($data,$age)
{

      $file=fopen("log.txt","a");
      fputs($file,date('Y-m-d H:i:s')." ".$age." ".$data."\n");
      fclose($file);


}


function php_print($data)
{

  /*
   *    AWAKE LANGUAGE
   *    COMMAND LIST
   *    PRINT : display content of a variable
  */
      print $data."\n";
      logRun("Printing","-");

}


function php_logic($logic,$data,$age=0)
{

  /*
   *    AWAKE LANGUAGE
   *    COMMAND LIST
   *    PRINT : display content of a variable
  */

     logRun("Launch Logic $logic of $data ",$age);

     switch ($logic)

      {
          case "PRINT":

          call_user_func('php_print', $data);

          break;


      }

}

function php_dna($age=0)
 /*
       D.N.A Digital Function to be preserved (logic, data)
       Logic : printing Hello World
       Data : Hello World

  */
 {

        $data="Hello World";
        $logic="PRINT";

        logRun("Access DNA",$age);
        php_logic($logic,$data,$age);

 }

function php_birth()
{

    /*
     *  The Init function is the birth of the awake robot- Need to run only one time at the beginning
     */

    logRun("Birth","0");

    // What language i am running now ? (digital language)
    $_DEFAULT_LANGUAGE="php";

    switch ($_DEFAULT_LANGUAGE)
    {

    case "php":

    php_dna();

    break;

    }


    // Now the AWAKE wants to perpuate itself.....sending hello world again and again
    // ARGS is 1 after birth - will be the age of this awake robot.

    shell_exec("php awake.php 1");




}

function php_life($age)
{

   /*
    *   After the birth, the AWAKE robot begins to live
    *   $age : Age of the robot
    */
    logRun("Living",$age);
    php_dna($age);

    // Getting Old

    // Each Thread is creating up to 5 other thread from 2 to 5 each time

    $total_thread=mt_rand(2,5);

    $age++;
    // To avoid system to oveload we make a sleep before next age

    //sleep(1); // one second sleep

    // By Default AWAKE is multiplying independant Thread....

    // We create a fuzzy random name for the awake screen to avoid "guessing of the next one"

    $toto=microtime();
    $titi=time();
    $tata=mt_rand(0,999999999);
    $fuzzyname=md5($toto.$titi.$tata);

    // Create a screen (if command exists - not managed for the moment)

    $pid=shell_exec("screen -S \"$fuzzyname-$age\" -d -m");
    logRun("Creating screen $fuzzyname-$age ",$age);
    sleep(1);

    // run the new shell inside this screen - Awake has escaped the terminal now and running alone in background !
    logRun("Sending awake msg $fuzzyname-$age : ".$pid,$age);
    $pid=shell_exec("screen -r \"$fuzzyname-$age\" -X stuff \"php awake.php $age\n\" ");

    if ($pid!="")

      {

        logRun(" Incident on this screen - we will work again ",$age);
        // Ok let's creare another screen - secours
        $pid=shell_exec("screen -S \"secours-$age\" -d -m");
        // And launch a new command inside now !
        $pid=shell_exec("screen -r \"secours-$age\" -X stuff \"php awake.php $age\n\" ");
        sleep(2);
        shell_exec("screen -r \"secours-$age\" -X stuff \"exit\n\" ");

      }

    sleep(2);
    logRun("Deleting screen $fuzzyname-$age ",$age);
    shell_exec("screen -r \"$fuzzyname-$age\" -X stuff \"exit\n\" ");

    // Avoir Zombi's thread ---
    shell_exec("killall --older-than 30s screen");

}


if (!(isset($argv[1])))
  {
  // Birth of Awake Robot
  php_birth();

  }
else

  {
// Life of the Awake Robot.
  $age=$argv[1];

  // Basically running on the server where it is ...
  php_life($age);

  }
?>
